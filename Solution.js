// Select the h1 element and store it in a variable named heading.

let heading = document.querySelector('h1');
console.log(heading);

// Check the typeof heading and log it.

console.log(typeof (heading));

// Change the color of heading to black.

heading.style.color = "black";

// Select all the hr elements and store it in a variable named allHrs using querySelectorAll

let allHrs = document.querySelectorAll('hr');

console.log(allHrs);

/* 
Convert the NodeList returned by querySelectorAll to Array using Array.from() or spread operator and store it in allHrsArray
Array.from converts an array kind of data into array so we can use methods like map, reduce
HINT:
let allHrsArray = Array.from(allHrs)
*/

let allHrsArray = Array.from(allHrs);

console.log(allHrsArray);

allHrsArray.forEach((x)=>{
    x.style.border = "1px solid tomato";  // Set the border of the all the hr elements to "1px solid tomato"
    x.style.background = "antiquewhite";  // Change the background of all the hr to "antiquewhite" using for of loop.
    x.style.borderRadius = '5px';         // Change the 'border-radius' of all the hr to "5px" using array.
    x.style.borderStyle = 'dashed';       // Change the border of all the hr element from solid to dashed type
})

// Change the alignment of the heading(h1) to center.

heading.style.textAlign = "center";

// Change the font size of the heading to 3rem.


heading.style.fontSize = "3rem";

// Change the border of hr with class 'image' to `2px solid purple`.

let reqhr = document.querySelectorAll(".image");

let reqhrArray = Array.from(reqhr);

reqhrArray.forEach((x)=>{
    x.style.border = "2px solid purple";
})

// Hide the box number 17 (last box).

let lastbox = document.querySelector(".seventeen");

lastbox.style.display = "none";

// Create a pragraph element and store it in variable named 'para' using `createElement`

let para = document.createElement('p');

// Change the inner text of para to "querySelector returns an element you can maupulate but querySelectorAll returns the collection of elements in array kind of structure."


para.innerText = "querySelector returns an element you can maupulate but querySelectorAll returns the collection of elements in array kind of structure.";

// Remove all the elements from box 1

let boxOne = document.querySelector('.one');

let childrenOfboxOne = boxOne.children;

let childrenOfboxOneArray = Array.from(childrenOfboxOne);

childrenOfboxOneArray.forEach((x)=>{
    x.remove();
})

// Replace all the elements inside box 1 with the para (you created above)

boxOne.append(para);

// Walking down the dom

let sixteen = document.querySelector('.sixteen');

let sixteenParent = sixteen.parentNode;

console.log(sixteenParent);

let sixteenChild = sixteen.childNodes;

console.log(sixteenChild);

let sixteenPreviousSibling = sixteen.previousSibling;

console.log(sixteenPreviousSibling);

let sixteenNextSibling = sixteen.nextSibling;

console.log(sixteenNextSibling);

let sixteenFirstChild = sixteen.firstChild;

console.log(sixteenFirstChild);

let sixteenLastChild = sixteen.lastChild;

console.log(sixteenLastChild);

let sixteenPreviousElement = sixteen.previousElementSibling;

console.log(sixteenPreviousElement);

let sixteenNextElement = sixteen.nextElementSibling;

console.log(sixteenNextElement);

let sixteenFirChildEle = sixteen.firstElementChild;

console.log(sixteenFirChildEle);

let sixteenlastChildEle = sixteen.lastElementChild;

console.log(sixteenlastChildEle);

// element is an html element where as node can be anything it can be even comment in html and text in html.

let box2 = document.querySelector('.two');

let paraInBox2 = document.createElement('p');

paraInBox2.innerText ="Append inserts as last child";

box2.append(paraInBox2);

let firstparaInBox2 = document.createElement('p');

firstparaInBox2.innerText ="Prepend inserts as first child";

box2.prepend(firstparaInBox2);

let boxFour = document.querySelector('.four');

boxFour.style.border = '1px solid black';

let boxFive = document.querySelector('.five');

boxFive.style.borderRadius = '10px';

let boxSix = document.querySelector('.six');

boxSix.style.color = 'black';

let paraInBox1 = boxOne.querySelector('p');

paraInBox1.style.fontSize = '0.8rem';

let parentOfAllBoxes = document.querySelector('.archive');

let boxes = Array.from(parentOfAllBoxes.children);

for (let box = 1 ; box < boxes.length ; box++) {
    if (box%2 == 1) {
        boxes[box-1].style.background = 'aliceblue';
    }
}

boxSix.classList.add("awesome-box");

box2.classList.toggle("awesome-box");

boxFour.classList.remove("awesome-box");

let bodyElement = document.querySelector('body');

bodyElement.style.background = 'bisque';

let btn = document.createElement('button');

btn.textContent = 'Click Me';

btn.style.background = 'oldlace';

btn.style.fontSize = '1rem';

let boxNine = document.querySelector('.nine');

boxNine.append(btn);

let imgElm = document.createElement('img');

imgElm.setAttribute('src','https://images.unsplash.com/photo-1592500595497-d1f52a40b207?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80')

let boxSeven = document.querySelector('.seven');

let childrenOfSeven = Array.from(boxSeven.children);

childrenOfSeven.forEach((x)=>{
    x.remove();
})

boxSeven.append(imgElm);

imgElm.style.width = '100%';
imgElm.style.height = '100%';


let userInput = document.createElement('input');
userInput.placeholder ="Enter you email!";
boxFive.append(userInput);

let anchorOne = document.createElement('a');
let anchorTwo = document.createElement('a');

anchorOne.innerText ='AltCampus';
anchorTwo.innerText ='Google';

anchorOne.href = 'https://www.mountblue.io/';
anchorTwo.href = 'https://google.com';

// anchorOne.setAttribute('href','https://www.mountblue.io/');
// anchorTwo.setAttribute('href','https://google.com');

boxFive.append(anchorOne);
boxFive.append(anchorTwo);



